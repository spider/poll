#!/bin/bash

##### COMMAND LINE VARIABLES ###############
# Environment
# Ex:  dev |  uat  |  prod
env=$1
# Server port
# Ex: 8090 |  8091 |  8092
serverPort=$2
# Project name, deploy folder name and jar name
# Ex. poll-api
projectName=$3
############################################

#### CONFIGURABLE VARIABLES ################
# $WORKSPACE is a jenkins var (/var/lib/jenkins/workspace/job_name)
# and source (from project repo)
projectJarDirSrc=$WORKSPACE/backend
projectJarSrc=$projectJarDirSrc/target/$projectName*.jar
projectResourcesDirSrc=$projectJarDirSrc/src/main/resources

# destination absolute path ( must be pre-created )
projectDir=$WORKSPACE/../../servers/$projectName/$env
projectJar=$projectDir/$projectName.jar
projectResourcesDir=$projectDir/resources

projectLogFile=$projectDir/server.log
projectPort=--server.port=$serverPort
projectProfile=--spring.profiles.active=$env
projectProperties=--spring.config.additional-location=file:$projectResourcesDir/
############################################

##### FUNCTIONS ############################
function stopServer(){
    echo "1 Stopping process on port: $serverPort"

    fuser -n tcp -k $serverPort > redirection &

    echo "1/5 Stopping DONE"
    echo " "
}

function deleteFiles(){
    echo "2 Removing previous version"

    echo "Removing jar: $projectJar"
    rm -rf $projectJar

    echo "Removing resources $projectResourcesDir"
    rm -rf $projectResourcesDir

    echo "Removing log file $projectLogFile"
    rm -rf $projectLogFile

    echo "2/5 Removing DONE"
    echo ""
}

function copyFiles(){
    echo "3 Copying new version files from $projectJarDirSrc to $projectDir"

    echo "Copying jar file $projectJarSrc to $projectJar"
    cp $projectJarSrc $projectJar

    echo "Copying resources files from $projectResourcesDirSrc to $projectResourcesDir"
    cp -r $projectResourcesDirSrc $projectResourcesDir

    echo "Creating log file $projectLogFile"
    touch $projectLogFile

    echo "3/5 Copying DONE"
    echo ""
}

function changeFilePermission(){
    echo "Changing File Permission"

    echo "Changing File Permission: chmod -R 0777 $projectDir"
    chmod -R 0777 $projectDir

    echo "Changing File Permission: chmod 0555 $projectJar"
    chmod 0555 $projectJar

    echo "Changing File Permission: chmod 0666 $projectLogFile"
    chmod 0666 $projectLogFile

    echo "DONE"
    echo " "
}

function run(){
   echo "4 Running $projectName on port $serverPort"
   nohup nice java -jar $projectJar $projectPort $projectProfile $projectProperties > $projectLogFile 2>&1 &
   echo "EXECUTE: nice java -jar $projectJar $projectPort $projectProfile $projectProperties $> $projectLogFile 2>&1 &"
   echo "4/5 Running DONE"
   echo " "
}

function watch(){
    tail -f $projectLogFile |
        while IFS= read line
            do
                echo "Buffering: " "$line"

                if [[ "$line" == *"Started "* ]]; then
                    echo "5/5 new version of" $projectName " started on " $serverPort " DONE"
                    pkill  tail
                fi
        done
}

##### FUNCTIONS CALLS ######################
# Usage: this_file.sh (Args: environment | port | project_name | external_resource)
# Jenkins:
# BUILD_ID=dontKillMe /path/to/this/file/api-deploy.sh dev 8082 spring-boot application-localhost.yml

stopServer            # 1/5 - stop server on port ...
deleteFiles           # 2/5 - delete destinations folder content
copyFiles             # 3/5 - copy files to deploy dir
changeFilePermission  #       and change permissions
run                   # 4/5 - start server
watch                 # 5/5 - watch loading messages until !!Started!! message is found
