CREATE TABLE `POLL`
(
    `id`          bigint(20)   NOT NULL AUTO_INCREMENT,
    `name`        varchar(512) NOT NULL,
    `description` varchar(1024) DEFAULT NULL,
    `type`        varchar(128)  DEFAULT NULL,
    `created_by`  bigint(20)    DEFAULT NULL,
    `created_at`  datetime      DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_poll_USER_ID` (`created_by`),
    CONSTRAINT `fk_poll_USER_ID` FOREIGN KEY (`created_by`) REFERENCES `USER` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `QUESTION`
(
    `id`       bigint(20)   NOT NULL AUTO_INCREMENT,
    `question` varchar(512) NOT NULL,
    `poll_id`  bigint(20)   NOT NULL,
    PRIMARY KEY (`id`),
    KEY `QUESTION_FK_POLL` (`poll_id`),
    CONSTRAINT `QUESTION_FK_POLL` FOREIGN KEY (`poll_id`) REFERENCES `POLL` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `ANSWER_OPTION`
(
    `id`          bigint(20)   NOT NULL AUTO_INCREMENT,
    `answer`      varchar(255) NOT NULL,
    `question_id` bigint(20)   NOT NULL,
    PRIMARY KEY (`id`),
    KEY `ANSWER_OPTION_FK_QUESTION` (`question_id`),
    CONSTRAINT `ANSWER_OPTION_FK_QUESTION` FOREIGN KEY (`question_id`) REFERENCES `QUESTION` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `VOTE`
(
    `id`          bigint(20) NOT NULL AUTO_INCREMENT,
    `poll_id`     bigint(20) NOT NULL,
    `question_id` bigint(20) NOT NULL,
    `answer_id`   bigint(20) NOT NULL,
    `created_by`  bigint(20) NOT NULL,
    `created_at`  datetime   NOT NULL,
    PRIMARY KEY (`id`),
    KEY `VOTE_FK_USER` (`created_by`),
    KEY `VOTE_FK_ANSWER` (`answer_id`),
    KEY `VOTE_FK_POLL` (`poll_id`),
    KEY `VOTE_FK` (`question_id`),
    CONSTRAINT `VOTE_FK` FOREIGN KEY (`question_id`) REFERENCES `QUESTION` (`id`) ON DELETE CASCADE,
    CONSTRAINT `VOTE_FK_ANSWER` FOREIGN KEY (`answer_id`) REFERENCES `ANSWER_OPTION` (`id`) ON DELETE CASCADE,
    CONSTRAINT `VOTE_FK_POLL` FOREIGN KEY (`poll_id`) REFERENCES `POLL` (`id`) ON DELETE CASCADE,
    CONSTRAINT `VOTE_FK_USER` FOREIGN KEY (`created_by`) REFERENCES `USER` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE OR REPLACE VIEW `VOTE_RESULT` AS
select
    `v`.`poll_id` AS `poll_id`,
    `p`.`name` AS `name`,
    `v`.`question_id` AS `question_id`,
    `q`.`question` AS `question`,
    `v`.`answer_id` AS `answer_id`,
    `ao`.`answer` AS `answer`,
    count(`v`.`answer_id`) AS `votes`
from
    (((`VOTE` `v`
        join `POLL` `p` on
            ((`v`.`poll_id` = `p`.`id`)))
        join `QUESTION` `q` on
            ((`v`.`question_id` = `q`.`id`)))
        join `ANSWER_OPTION` `ao` on
        ((`v`.`answer_id` = `ao`.`id`)))
group by
    `v`.`poll_id`,
    `v`.`question_id`,
    `v`.`answer_id`;
