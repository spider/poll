create table hibernate_sequence
(
    next_val bigint(20) DEFAULT NULL
);

CREATE TABLE `ROLE`
(
    `id`   bigint(20)   NOT NULL AUTO_INCREMENT,
    `name` varchar(128) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ROLE_UN` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `USER`
(
    `id`         bigint(20)   NOT NULL AUTO_INCREMENT,
    `login`      varchar(128) NOT NULL,
    `password`   varchar(128) NOT NULL,
    `email`      varchar(128) DEFAULT NULL,
    `activated`  boolean      DEFAULT false,
    `first_name` varchar(128) DEFAULT NULL,
    `last_name`  varchar(128) DEFAULT NULL,
    `created_by`  bigint(20)  NOT NULL,
    `created_at`  datetime    NOT NULL,
    `last_modified_by`  bigint(20)  DEFAULT NULL,
    `last_modified_at`  datetime    DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `USER_FK_CREATED_BY` (`created_by`),
    CONSTRAINT `USER_FK_CREATED_BY` FOREIGN KEY (`created_by`) REFERENCES `USER` (`id`),
    KEY `USER_FK_UPDATED_BY` (`last_modified_by`),
    CONSTRAINT `USER_FK_UPDATED_BY` FOREIGN KEY (`last_modified_by`) REFERENCES `USER` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE `USER_ROLE`
(
    `user_id` bigint(20) NOT NULL,
    `role_id` bigint(20) NOT NULL,
    PRIMARY KEY (`user_id`, `role_id`),
    CONSTRAINT `USER_ROLE_FK_USER` FOREIGN KEY (`user_id`) REFERENCES `USER` (`id`),
    CONSTRAINT `USER_ROLE_FK_ROLE` FOREIGN KEY (`role_id`) REFERENCES `ROLE` (`id`)

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
