insert into ROLE values (1, 'ROLE_USER');
insert into ROLE values (2, 'ROLE_ADMIN');

insert into USER values (1, 'system', '$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG', 'system@localhost', true, 'System', 'System', 1, '2020-01-01T00:00:00', null, null);
insert into USER values (2, 'anonymoususer', '$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO', 'anonymous@localhost', true, 'Anonymous', 'Anonymous', 1, '2020-01-01T00:00:00', null, null);
insert into USER values (3, 'user', '$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K', 'user@localhost.com', true, 'User', 'User', 1, '2020-01-01T00:00:00', null, null);
insert into USER values (4, 'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'admin@localhost.com', true,  'Administrator', 'Administrator', 1, '2020-01-01T00:00:00', null, null);

insert into USER_ROLE values (3, 1);
insert into USER_ROLE values (4, 1);
insert into USER_ROLE values (4, 2);
