package com.howto.poll.model.question;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.howto.poll.model.option.AnswerOptionDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@EqualsAndHashCode(of = {"id", "question"})
@ToString(of = {"id", "question"})
public class QuestionDto {

    @JsonProperty(access = READ_ONLY)
    private Long id;

    @JsonProperty(access = READ_WRITE)
    private String question;

    @JsonProperty(access = READ_WRITE)
    private Set<AnswerOptionDto> answerOptions = new HashSet<>();
}
