package com.howto.poll.model.user;

import com.howto.poll.repository.user.User;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    User convertToEntity(UserDtoExtended dto);

    Collection<User> convertToEntity(Collection<UserDto> dtos);

    @Mapping(source = "roles", target = "authorities")
    UserDtoExtended convertToDTOExtended(User entity);

    @Named(value = "convertToDTO")
    UserDto convertToDTO(User entity);

    @IterableMapping(qualifiedByName = "convertToDTO")
    Collection<UserDto> convertToDTO(Collection<User> entities);

    @IterableMapping(qualifiedByName = "convertToDTOExtended")
    Collection<UserDtoExtended> convertToDTOExtended(Collection<User> entities);
}
