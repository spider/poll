package com.howto.poll.model.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(of = {"username", "email"})
@ToString(of = {"username", "email"})
public class UserDto {

    private Long id;

    @NotNull
    private String username;

    private String password;

    private String firstName;

    private String lastName;

    @NotNull
    private String email;

    private Boolean activated;

//    TODO: createdBy - modifiedBy
}
