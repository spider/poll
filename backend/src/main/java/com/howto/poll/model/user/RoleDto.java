package com.howto.poll.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.howto.poll.repository.user.Roles;
import lombok.Data;

import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_WRITE;

@Data
public class RoleDto {

    @NotNull
    @JsonProperty(access = READ_WRITE)
    private Roles name;
}
