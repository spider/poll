package com.howto.poll.model.option;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@EqualsAndHashCode(of = {"id", "answer"})
@ToString(of = {"id", "answer"})
public class AnswerOptionDto {

    @JsonProperty(access = READ_ONLY)
    private Long id;

    @JsonProperty(access = READ_WRITE)
    private String answer;

    @JsonProperty(access = READ_ONLY)
    private Long votes;
}
