package com.howto.poll.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data @AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode(callSuper = true, exclude = {"authorities"})
public class UserDtoExtended extends UserDto {

    private Set<RoleDto> authorities = new HashSet<>();
}
