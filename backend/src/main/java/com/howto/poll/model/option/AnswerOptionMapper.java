package com.howto.poll.model.option;

import com.howto.poll.repository.option.AnswerOption;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface AnswerOptionMapper {

    @Mapping(target = "id", ignore = true)
    AnswerOption convertToEntity(AnswerOptionDto dto);

    Collection<AnswerOption> convertToEntity(Collection<AnswerOptionDto> dtos);

    AnswerOptionDto convertToDTO(AnswerOption entity);

    Collection<AnswerOptionDto> convertToDTO(Collection<AnswerOption> entities);
}
