package com.howto.poll.model.poll;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.howto.poll.model.question.QuestionDto;
import com.howto.poll.model.user.UserDto;
import com.howto.poll.repository.user.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.*;

@Data
@EqualsAndHashCode(of = {"id", "name"})
@ToString(of = {"id", "name"})
public class PollDto {

    @JsonProperty(access = READ_ONLY)
    private Long id;

    @JsonProperty(access = READ_WRITE)
    private String name;

    @JsonProperty(access = READ_WRITE)
    private String description;

    @JsonProperty(access = READ_WRITE)
    private String type;

    @JsonProperty(access = READ_WRITE)
    private Set<QuestionDto> questions = new HashSet<>();

    @JsonProperty(access = READ_ONLY)
    private UserDto createdBy;

    @JsonProperty(access = READ_ONLY)
    private ZonedDateTime creationTimestamp;
}
