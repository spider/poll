package com.howto.poll.model.poll;

import com.howto.poll.repository.poll.Poll;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface PollMapper {

    @Mapping(target = "id", ignore = true)
    Poll convertToEntity(PollDto dto);

    Collection<Poll> convertToEntity(Collection<PollDto> dtos);

    PollDto convertToDTO(Poll entity);

    Collection<PollDto> convertToDTO(Collection<Poll> entities);
}
