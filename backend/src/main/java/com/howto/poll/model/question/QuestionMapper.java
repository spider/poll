package com.howto.poll.model.question;

import com.howto.poll.repository.question.Question;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface QuestionMapper {

    @Mapping(target = "id", ignore = true)
    Question convertToEntity(QuestionDto dto);

    Collection<Question> convertToEntity(Collection<QuestionDto> dtos);

    QuestionDto convertToDTO(Question entity);

    Collection<QuestionDto> convertToDTO(Collection<Question> entities);
}
