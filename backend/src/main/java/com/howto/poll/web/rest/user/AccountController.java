package com.howto.poll.web.rest.user;

import com.howto.poll.model.user.UserDtoExtended;
import com.howto.poll.model.user.UserMapper;
import com.howto.poll.repository.user.User;
import com.howto.poll.repository.user.UserRepository;
import com.howto.poll.service.MailService;
import com.howto.poll.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AccountController {

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountController.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final UserMapper userMapper;

    private final MailService mailService;
//
//    private final PersistentTokenRepository persistentTokenRepository;

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/register")
    public void registerAccount(@Valid @RequestBody UserDtoExtended managedUserVM) {
        User user = userService.registerUser(userMapper.convertToEntity(managedUserVM),
            managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = Optional.empty();//userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public UserDtoExtended getAccount() {
        return userService.getUserWithAuthorities()
            .map(userMapper::convertToDTOExtended)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }
//
//    /**
//     * {@code POST  /account} : update the current user information.
//     *
//     * @param userDTO the current user information.
//     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
//     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user login wasn't found.
//     */
//    @PostMapping("/account")
//    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
//        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
//        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
//        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
//            throw new EmailAlreadyUsedException();
//        }
//        Optional<User> user = userRepository.findOneByLogin(userLogin);
//        if (!user.isPresent()) {
//            throw new AccountResourceException("User could not be found");
//        }
//        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
//            userDTO.getLangKey(), userDTO.getImageUrl());
//    }
//
//    /**
//     * {@code GET  /account/sessions} : get the current open sessions.
//     *
//     * @return the current open sessions.
//     * @throws RuntimeException {@code 500 (Internal Server Error)} if the current open sessions couldn't be retrieved.
//     */
//    @GetMapping("/account/sessions")
//    public List<PersistentToken> getCurrentSessions() {
//        return persistentTokenRepository.findByUser(
//            userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()
//                .orElseThrow(() -> new AccountResourceException("Current user login not found")))
//                    .orElseThrow(() -> new AccountResourceException("User could not be found"))
//        );
//    }
//
//    /**
//     * {@code DELETE  /account/sessions?series={series}} : invalidate an existing session.
//     *
//     * - You can only delete your own sessions, not any other user's session
//     * - If you delete one of your existing sessions, and that you are currently logged in on that session, you will
//     *   still be able to use that session, until you quit your browser: it does not work in real time (there is
//     *   no API for that), it only removes the "remember me" cookie
//     * - This is also true if you invalidate your current session: you will still be able to use it until you close
//     *   your browser or that the session times out. But automatic login (the "remember me" cookie) will not work
//     *   anymore.
//     *   There is an API to invalidate the current session, but there is no API to check which session uses which
//     *   cookie.
//     *
//     * @param series the series of an existing session.
//     * @throws UnsupportedEncodingException if the series couldn't be URL decoded.
//     */
//    @DeleteMapping("/account/sessions/{series}")
//    public void invalidateSession(@PathVariable String series) throws UnsupportedEncodingException {
//        String decodedSeries = URLDecoder.decode(series, "UTF-8");
//        SecurityUtils.getCurrentUserLogin()
//            .flatMap(userRepository::findOneByLogin)
//            .ifPresent(u ->
//                persistentTokenRepository.findByUser(u).stream()
//                    .filter(persistentToken -> StringUtils.equals(persistentToken.getSeries(), decodedSeries))
//                    .findAny().ifPresent(t -> persistentTokenRepository.deleteById(decodedSeries)));
//    }

}
