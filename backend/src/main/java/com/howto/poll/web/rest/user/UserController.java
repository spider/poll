package com.howto.poll.web.rest.user;

import com.howto.poll.model.user.UserDto;
import com.howto.poll.model.user.UserDtoExtended;
import com.howto.poll.model.user.UserMapper;
import com.howto.poll.repository.user.User;
import com.howto.poll.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    @PostMapping
    public UserDto createUser(@Valid @RequestBody UserDtoExtended user) {
        return userMapper.convertToDTO(
                userService.save(userMapper.convertToEntity(user)));
    }

    @GetMapping
    public Collection<UserDtoExtended> getUsers() {
        return userMapper.convertToDTOExtended(
                userService.getAll());
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@PathVariable("id") Long id){
        return userMapper.convertToDTO(
                userService.getById(id));
    }

    @GetMapping("/by/{username}")
    public UserDto getUserByName(@PathVariable("username") String username, @RequestParam(required = false) boolean withRoles) {
        User user = userService.getByName(username, withRoles);

        return withRoles ? userMapper.convertToDTOExtended(user) : userMapper.convertToDTO(user);
    }

    @PutMapping("/{id}")
    public UserDto updateUser(@PathVariable("id") Long id, @Valid @RequestBody UserDtoExtended user) {
        return userMapper.convertToDTO(
                userService.updateUser(id, userMapper.convertToEntity(user)));
    }

    @PutMapping("/{id}/role")
    public UserDto updateUser(@PathVariable("id") Long id, @RequestParam Set<String> roles) {
        return userMapper.convertToDTO(
                userService.updateRoles(id, roles));
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @PutMapping("/{id}/active")
    public UserDto softDeleteUser(@PathVariable("id") Long id, @RequestParam boolean active) {
        //TODO: Activate if exists, deactivate instead of delete
        return userMapper.convertToDTO(
                userService.updateActive(id, active));
    }
}
