package com.howto.poll.web.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* com.howto.poll.web.rest..*.*(..))")
    public void anyControllersMethod() {
    }

    @Before("anyControllersMethod()")
    public void loggingBefore(JoinPoint joinPoint) {
        String method = joinPoint.getSignature().toShortString();
        Object[] args = joinPoint.getArgs();

        if (args == null || args.length == 0) {
            log.info("{} start processing", method);
            return;
        }

        int i = 1;
        log.info("{} start processing with parameters:", method);
        for (Object param : args) {
            log.info("\t{} parameter is {}", i++, param);
        }
    }

    @AfterReturning(pointcut = "anyControllersMethod()", returning = "retVal")
    public void logAfterMethod(JoinPoint joinPoint, Object retVal) {
        log.info("{} completed successfully ", joinPoint.getSignature().toShortString());
        if (retVal != null) {
            if (retVal instanceof Collection) {
                int collectionResultLogLimit = 5;
                log.info("with result: {}", ((Collection) retVal).stream()
                        .limit(collectionResultLogLimit).collect(Collectors.toList()));
                log.info(((Collection) retVal).size() > collectionResultLogLimit ?
                        " and other: total size: " + ((Collection) retVal).size() : "");
            } else {
                log.info("with result: {}", retVal);
            }
        }
    }

    @AfterThrowing(pointcut = "anyControllersMethod()", throwing = "e")
    public void logAfterThrowingAllMethods(JoinPoint joinPoint, Exception e) {
        log.error("{} failed with exception: ", joinPoint.getSignature().toShortString());
        log.error(e.getMessage());
    }
}
