package com.howto.poll.web.rest.option;

import com.howto.poll.model.poll.PollMapper;
import com.howto.poll.service.vote.VoteService;
import com.howto.poll.model.poll.PollDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/votes")
public class VoteController {

    private final VoteService voteService;

    private final PollMapper pollMapper;

    @PostMapping
    public PollDto voteForPollOption(@RequestParam Long pollId, @RequestBody Collection<VoteDto> votes) {
        return pollMapper.convertToDTO(
                voteService.voteForPollOption(pollId, votes));
    }
}
