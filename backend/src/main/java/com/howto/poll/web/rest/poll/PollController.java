package com.howto.poll.web.rest.poll;

import com.howto.poll.model.poll.PollDto;
import com.howto.poll.model.poll.PollMapper;
import com.howto.poll.service.poll.PollService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/polls")
public class PollController {

    private final PollService pollService;

    private final PollMapper pollMapper;

    @GetMapping
    public Collection<PollDto> getAllPolls() {
        return pollMapper.convertToDTO(
                pollService.getAll());
    }

    @PostMapping
    public PollDto createPoll(@RequestBody PollDto poll, @RequestParam("userId") long userId) {
        return pollMapper.convertToDTO(
                pollService.save(userId, pollMapper.convertToEntity(poll)));
    }

    @GetMapping("/{id}")
    public PollDto getPoll(@PathVariable long id) {
        return pollMapper.convertToDTO(
                pollService.getById(id));
    }

    @DeleteMapping("/{id}")
    public void deletePoll(@PathVariable long id) {
        pollService.delete(id);
    }
}
