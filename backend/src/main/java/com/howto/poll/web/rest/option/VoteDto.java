package com.howto.poll.web.rest.option;

import lombok.Data;

@Data
public class VoteDto {

    Long questionId;

    Long answerId;
}
