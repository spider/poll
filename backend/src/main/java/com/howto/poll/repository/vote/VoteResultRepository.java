package com.howto.poll.repository.vote;

import org.springframework.data.repository.CrudRepository;


public interface VoteResultRepository extends CrudRepository<VoteResult, Long> {
}
