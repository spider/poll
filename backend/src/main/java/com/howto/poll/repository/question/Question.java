package com.howto.poll.repository.question;

import com.howto.poll.repository.option.AnswerOption;
import com.howto.poll.repository.poll.Poll;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode(exclude = {"poll", "answerOptions"})
@ToString(exclude = {"poll", "answerOptions"})
@Entity @Table(name = "QUESTION")
public class Question {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "question", nullable = false, updatable = false)
    private String question;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.JOIN)
    private Set<AnswerOption> answerOptions = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "poll_id")
    private Poll poll;
}
