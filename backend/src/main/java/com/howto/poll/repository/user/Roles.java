package com.howto.poll.repository.user;

public enum Roles {
    ROLE_USER, ROLE_ADMIN, ROLE_ANONYMOUS
}
