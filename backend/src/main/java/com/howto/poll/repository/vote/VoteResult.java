package com.howto.poll.repository.vote;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity @Table(name = "VOTE_RESULT")
public class VoteResult {

    @EmbeddedId
    private VoteResultId id;

    @Column(name = "votes", nullable = false, updatable = false)
    private Long votes;
}
