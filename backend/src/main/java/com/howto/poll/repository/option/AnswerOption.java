package com.howto.poll.repository.option;

import com.howto.poll.repository.question.Question;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode(of = {"answer"})
@ToString(of = {"answer"})
@Entity @Table(name = "ANSWER_OPTION")
public class AnswerOption {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "answer", nullable = false, updatable = false)
    private String answer;

    @Transient
    private Long votes;

    @ManyToOne @JoinColumn(name = "question_id")
    private Question question;
}
