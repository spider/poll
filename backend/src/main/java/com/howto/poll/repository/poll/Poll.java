package com.howto.poll.repository.poll;

import com.howto.poll.repository.question.Question;
import com.howto.poll.repository.user.User;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode(exclude = {"questions", "createdBy"})
@ToString(exclude = {"questions", "createdBy"})
@Entity @Table(name = "POLL")
public class Poll {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false, updatable = false)
    private String name;

    @Column(name = "description", nullable = true, updatable = false)
    private String description;

    @Column(name = "type")
    private String type;

    @OneToMany
    @JoinColumn(name = "poll_id")
    private Set<Question> questions = new HashSet<>();

    @CreatedBy
    @ManyToOne @JoinColumn(name = "created_by")
    private User createdBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private ZonedDateTime creationTimestamp;
}
