package com.howto.poll.repository.poll;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface PollRepository extends JpaRepository<Poll, Long> {

    @EntityGraph(attributePaths = "questions")
    Optional<Poll> findPollWithQuestionsById(Long id);

    default Poll getPollWithQuestionsById(Long id){
        return findPollWithQuestionsById(id)
                .orElseThrow(() -> new IllegalArgumentException("Poll with id " + id + " does not exist."));
    }
}
