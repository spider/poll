package com.howto.poll.repository.vote;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class VoteResultId implements Serializable {

    @Column(name = "poll_id", nullable = false, updatable = false)
    private Long pollId;

    @Column(name = "question_id", nullable = false, updatable = false)
    private Long questionId;

    @Column(name = "answer_id", nullable = false, updatable = false)
    private Long answerId;
}
