package com.howto.poll.repository.vote;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity @Table(name = "VOTE")
public class Vote {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "poll_id", nullable = false, updatable = false)
    private Long pollId;

    @Column(name = "question_id", nullable = false, updatable = false)
    private Long questionId;

    @Column(name = "answer_id", nullable = false, updatable = false)
    private Long answerId;

    @Column(name = "created_by")
    private Long created_by;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;
}
