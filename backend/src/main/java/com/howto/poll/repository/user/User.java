package com.howto.poll.repository.user;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Data @AllArgsConstructor @NoArgsConstructor
@EqualsAndHashCode(exclude = { "roles", "createdBy", "lastModifiedBy" })
@ToString(exclude = { "roles", "createdBy", "lastModifiedBy" })
@Entity @Table(name = "USER")
public class User {

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @NaturalId
    @Column(name = "login", length = 63, unique = true, nullable = false, updatable = false)
    private String username;

    @Column(name = "password", length = 63, nullable = false)
    private String password;

    @Email
    @Column(name = "email", length = 63, unique = true)
    private String email;

    @Column(name = "first_name", length = 63)
    private String firstName;

    @Column(name = "last_name", length = 63)
    private String lastName;

    @Column(name = "activated", nullable = false)
    private Boolean activated = Boolean.FALSE;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USER_ROLE",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private Set<Role> roles = new HashSet<>();

    @CreatedBy
    @ManyToOne @JoinColumn(name = "created_by")
    private User createdBy;

    @CreationTimestamp
    @Column(name = "created_at")
    private ZonedDateTime creationTimestamp;

    @LastModifiedBy
    @ManyToOne @JoinColumn(name = "last_modified_by")
    private User lastModifiedBy;

    @UpdateTimestamp
    @Column(name = "last_modified_at")
    private ZonedDateTime updateTimestamp;
}
