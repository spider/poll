package com.howto.poll.security;

import static com.howto.poll.repository.user.Roles.*;

public final class AuthoritiesConstants {

    public static final String ADMIN = ROLE_ADMIN.name();

    public static final String USER = ROLE_USER.name();

    public static final String ANONYMOUS = ROLE_ANONYMOUS.name();

    private AuthoritiesConstants() {
    }
}
