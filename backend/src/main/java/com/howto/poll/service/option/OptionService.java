package com.howto.poll.service.option;

import com.howto.poll.repository.option.AnswerOption;

public interface OptionService {

    AnswerOption findById(Long id);
}
