package com.howto.poll.service.user;

import com.howto.poll.exception.ApplicationException;
import com.howto.poll.repository.user.Role;
import com.howto.poll.repository.user.User;
import com.howto.poll.repository.user.UserRepository;
import com.howto.poll.security.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.howto.poll.repository.user.Roles.*;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User save(User user) {
        user.setId(null);
        user.getRoles().add(new Role(1L, ROLE_USER));

        //TODO: fix
        User createdBy = new User();
        createdBy.setId(1L);
        user.setCreatedBy(createdBy);
//        TODO: convert password
        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException("User not found by id " + id));
    }

    @Override
    public User getByName(String username, boolean withRoles) {
        Optional<User> user = withRoles ?
                userRepository.findOneWithRolesByUsername(username) :
                userRepository.findOneByUsername(username);

        return user.orElseThrow(() -> new ApplicationException("User not found by username " + username));
    }

    @Override
    public User updateUser(Long id, User user) {
        User storedUser = userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException("User not found by id " + id));

        if (user.getActivated() != storedUser.getActivated()) {
            throw new ApplicationException("Cannot modify user property: activated");
        }

        storedUser.setFirstName(user.getFirstName());
        storedUser.setLastName(user.getLastName());
        storedUser.setEmail(user.getEmail());
        return storedUser;
    }

    @Override
    public User updateRoles(Long id, Set<String> roles) {
        User storedUser = userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException("User not found by id " + id));

        //storedUser.setRoles(roles.stream().map(Role::new).collect(Collectors.toSet()));
        return storedUser;
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User updateActive(Long id, boolean active) {
        User storedUser = userRepository.findById(id)
                .orElseThrow(() -> new ApplicationException("User not found by id " + id));

        storedUser.setActivated(active);
        return storedUser;
    }

    @Override
    public User registerUser(User user, String password) {
        User userToActivate = userRepository.findOneWithRolesByUsername(user.getUsername().toLowerCase())
            .orElseThrow(IllegalArgumentException::new);
        userToActivate.setActivated(true);
//            .ifPresent(existingUser -> {
//                boolean removed = removeNonActivatedUser(existingUser);
//                if (!removed) {
//                    throw new UsernameAlreadyUsedException();
//                }
//        });
//        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
//            boolean removed = removeNonActivatedUser(existingUser);
//            if (!removed) {
//                throw new EmailAlreadyUsedException();
//            }
//        });
//        User newUser = new User();
//        String encryptedPassword = passwordEncoder.encode(password);
//        newUser.setLogin(userDTO.getLogin().toLowerCase());
//        // new user gets initially a generated password
//        newUser.setPassword(encryptedPassword);
//        newUser.setFirstName(userDTO.getFirstName());
//        newUser.setLastName(userDTO.getLastName());
//        if (userDTO.getEmail() != null) {
//            newUser.setEmail(userDTO.getEmail().toLowerCase());
//        }
//        newUser.setImageUrl(userDTO.getImageUrl());
//        newUser.setLangKey(userDTO.getLangKey());
//        // new user is not active
//        newUser.setActivated(false);
//        // new user gets registration key
//        newUser.setActivationKey(RandomUtil.generateActivationKey());
//        Set<Authority> authorities = new HashSet<>();
//        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
//        newUser.setAuthorities(authorities);
//        userRepository.save(newUser);
//        this.clearUserCaches(newUser);
//        log.debug("Created Information for User: {}", newUser);
//        return newUser;
        return save(userToActivate);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        Optional<User> user = SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneWithRolesByUsername);
        if (user.isPresent()) {
            user.get().getRoles().add(new Role(2L, ROLE_ADMIN));
            user.get().getRoles().add(new Role(1L, ROLE_USER));
        }
        return user;
    }

}
