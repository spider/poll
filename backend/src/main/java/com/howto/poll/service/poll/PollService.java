package com.howto.poll.service.poll;

import com.howto.poll.repository.poll.Poll;

import java.util.Collection;

public interface PollService {

    Collection<Poll> getAll();

    Poll save(Long userId, Poll poll);

    Poll getById(Long id);

    void delete(Long id);
}
