package com.howto.poll.service.vote;

import com.howto.poll.repository.poll.Poll;
import com.howto.poll.web.rest.option.VoteDto;

import java.util.Collection;

public interface VoteService {

    Poll voteForPollOption(Long pollId, Collection<VoteDto> optionAnswer);
}
