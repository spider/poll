package com.howto.poll.service.poll;

import com.howto.poll.repository.option.AnswerOptionRepository;
import com.howto.poll.repository.poll.Poll;
import com.howto.poll.repository.poll.PollRepository;
import com.howto.poll.repository.question.Question;
import com.howto.poll.repository.question.QuestionRepository;
import com.howto.poll.repository.user.User;
import com.howto.poll.repository.user.UserRepository;
import com.howto.poll.repository.vote.VoteRepository;
import com.howto.poll.repository.vote.VoteResultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class PollServiceImpl implements PollService {

    private final PollRepository pollRepository;

    private final AnswerOptionRepository answerOptionRepository;

    private final QuestionRepository questionRepository;

    private final UserRepository userRepository;

    private final VoteRepository voteRepository;

    private final VoteResultRepository voteResultRepository;

    @Override
    public Collection<Poll> getAll() {
        return pollRepository.findAll();
    }

    @Override
    public Poll save(Long userId, Poll poll) {
        User user = userRepository.findOneById(userId);
        poll.setCreatedBy(user);

        Poll savedPoll = pollRepository.save(poll);
        savedPoll.getQuestions().forEach(q -> {
            q.setPoll(savedPoll);
            Question savedQuestion = questionRepository.save(q);
            savedQuestion.getAnswerOptions().forEach(a -> {
                a.setQuestion(savedQuestion);
                answerOptionRepository.save(a);
            });
        });

        return savedPoll;
    }

    @Override
    public Poll getById(Long id) {
        return pollRepository.getPollWithQuestionsById(id);
    }

    @Override
    public void delete(Long id) {
        pollRepository.deleteById(id);
    }
}
