package com.howto.poll.service.option;

import com.howto.poll.repository.option.AnswerOption;
import com.howto.poll.repository.option.AnswerOptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class OptionServiceImpl implements OptionService {

    private final AnswerOptionRepository answerOptionRepository;

    @Override
    public AnswerOption findById(Long id) {
        return answerOptionRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Option with id " + id + " does not exist."));
    }
}
