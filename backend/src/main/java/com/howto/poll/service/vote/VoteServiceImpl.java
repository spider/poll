package com.howto.poll.service.vote;

import com.howto.poll.repository.option.AnswerOption;
import com.howto.poll.repository.option.AnswerOptionRepository;
import com.howto.poll.repository.poll.Poll;
import com.howto.poll.repository.question.Question;
import com.howto.poll.repository.question.QuestionRepository;
import com.howto.poll.repository.user.UserRepository;
import com.howto.poll.repository.vote.Vote;
import com.howto.poll.repository.vote.VoteRepository;
import com.howto.poll.service.poll.PollService;
import com.howto.poll.web.rest.option.VoteDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Collection;

@Service
@Transactional
@RequiredArgsConstructor
public class VoteServiceImpl implements VoteService {

    private final PollService pollService;

    private final AnswerOptionRepository answerRepository;
    private final QuestionRepository questionRepository;
    private final VoteRepository voteRepository;
    private final UserRepository userRepository;

    @Override
    public Poll voteForPollOption(Long pollId, Collection<VoteDto> optionAnswer) {
        Poll poll = pollService.getById(pollId);
        for (VoteDto vote: optionAnswer) {
            Question question = questionRepository.getOne(vote.getQuestionId());
            AnswerOption option = answerRepository.getOne(vote.getAnswerId());
            voteRepository.save(new Vote(null, poll.getId(), question.getId(), option.getId(),
                    userRepository.getOne(2L).getId(), ZonedDateTime.now()));
        }
        return poll;
    }
}
