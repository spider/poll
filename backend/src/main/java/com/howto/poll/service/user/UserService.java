package com.howto.poll.service.user;

import com.howto.poll.repository.user.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

    User save(User user);

    List<User> getAll();

    User getById(Long id);

    User getByName(String username, boolean withRoles);

    User updateUser(Long id, User user);

    User updateRoles(Long id, Set<String> roles);

    void delete(Long id);

    User updateActive(Long id, boolean active);

    User registerUser(User user, String password);

    Optional<User> getUserWithAuthorities();
}
