package com.howto.poll.repository.user;

import com.howto.poll.repository.poll.Poll;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@Ignore
@SpringBootTest
@ActiveProfiles("test")
//@DataJpaTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@Transactional(readOnly = true)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
//
//    private static Set<Role> userRoles = Sets.newSet(new Role("USER"));
//    private static Set<Role> adminRoles = Sets.newSet(new Role("USER"), new Role("ADMIN"));
//
//    private static Set<Poll> polls = new HashSet<>();
//    private static User newUser = new User(null, "test", "test","Test", "Test", "test@localhost.com", true, null, null, null, userRoles, polls);
//
//    private static User storedUser = new User(1L, "user", "user", "User", "User", "user@localhost.com", true,  null, null, null, userRoles, polls);
//    private static User storedAdmin = new User(2L, "admin", "admin", "Administrator", "Administrator", "admin@localhost.com", true, null, null, null, adminRoles, polls);
//
//    @Test
//    public void test01_createUser() {
//        User user = userRepository.save(newUser);
//        assertThat("Saved user is not equal to test user", user, equalTo(newUser));
//    }
//
//    @Test
//    public void test02_findUserById() {
//        User user = userRepository.findById(storedUser.getId()).get();
//        assertThat("Cannot find user by id = 1", user, equalTo(storedUser));
//
//        User admin = userRepository.findById(storedAdmin.getId()).get();
//        assertThat("Cannot find user by id = 1", admin, equalTo(storedAdmin));
//    }
//
//    @Test
//    public void test02_findUserByLogin() {
//        User user = userRepository.findOneByLogin(storedUser.getUsername()).orElseThrow(RuntimeException::new);
//        assertThat("Cannot find user by username = user", user, equalTo(storedUser));
//    }
//
//    @Test
//    public void test02_findOneWithRolesByUsername() {
//        User user = userRepository.findOneWithRolesByLogin(storedUser.getUsername()).orElseThrow(RuntimeException::new);
//        assertThat("Cannot find user with username = user", user, equalTo(storedUser));
//    }
//
//    @Test
//    public void test03_updateUser() {
//        User user = userRepository.save(newUser);
//        user.setFirstName("Test");
//
//        assertThat("Updated user is not equal to test user", user, equalTo(newUser));
//    }
//
//    @Test
//    public void test04_deleteUser() {
//        User user = userRepository.findById(storedUser.getId()).get();
//        assertThat("Cannot find user by id = 1", user, equalTo(storedUser));
//
//        userRepository.delete(storedUser);
//        assertThat("User was not removed", userRepository.findById(user.getId()), equalTo(Optional.empty()));
//    }
//
//    @Test
//    public void test05_deleteAdmin() {
//        User user = userRepository.findById(storedAdmin.getId()).get();
//        assertThat("Cannot find user by id = 2", user, equalTo(storedAdmin));
//
//        userRepository.delete(storedAdmin);
//        assertThat("User was not removed", userRepository.findById(user.getId()), equalTo(Optional.empty()));
//    }
}
