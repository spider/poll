import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/poll',
    pathMatch: 'full'
  },
  {
    path: 'authentication',
    loadChildren: () => import('./security/security.module')
      .then(m => m.SecurityModule)
  },
  {
    path: 'admin',
    // data: {
    //   authorities: ['ROLE_ADMIN']
    // },
    // canActivate: [UserRouteAccessService],
    loadChildren: () => import('./domain/admin/admin-routing.module')
      .then(m => m.AdminRoutingModule)
  },
  {
    path: 'poll',
    loadChildren: () => import('./domain/poll/poll.module')
      .then(m => m.PollModule)
  },
  // TODO: add home - domain modules
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
