import { UserState } from '../domain/user/store/user.state';

export const AppState = [
  UserState,
];

export * from '../domain/user/store/user.actions';
export * from '../domain/user/store/user.state';
