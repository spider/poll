import { Component, OnInit, OnDestroy } from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {AuthenticationService} from '../../security/authentication/services/authentication.service';
import {AccountService} from '../../security/authentication/services/account.service';
import {tap} from 'rxjs/operators';
import {LoginService} from '../../security/authentication/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  isAuthenticated = false;
  private username: string;
  // private principalSubscription$: Observable<Account>;
  private principalSubscription$: Subscription;



  constructor(private accountService: AccountService,
              private loginService: LoginService) {}

  ngOnInit() {
    this.principalSubscription$ = this.accountService.getPrincipal()
      .pipe(
        tap((a) => {
          this.isAuthenticated = a != null;
          if (!!a) {
            this.username = a.firstName;
          }
        })
      )
      .subscribe();
  }

  onLogout() {
    console.log('logout');
    this.loginService.logout();
  }

  ngOnDestroy() {
    this.principalSubscription$.unsubscribe();
  }
}
