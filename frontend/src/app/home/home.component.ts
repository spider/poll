import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AccountService } from '../security/authentication/services/account.service';
import { PrincipalAccount } from '../security/authentication/account.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: PrincipalAccount | null = null;
  authSubscription?: Subscription;

  constructor(private accountService: AccountService) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getPrincipal().subscribe(account => (this.account = account));
  }

  isAuthenticated(): boolean {
    return true; // this.accountService.isAuthenticated();
  }

  login(): void {
    // this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
