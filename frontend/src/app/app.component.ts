import { Component, OnInit } from '@angular/core';

import { LoggingService } from './shared/services/logging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private log: LoggingService
  ) {}

  ngOnInit() {
    this.log.info('Hello from AppComponent ngOnInit');
  }
}
