import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { PollService } from './poll/poll.service';
import { VoteService } from './poll/vote.service';
import { AuthInterceptorService } from '../security/interceptor/auth-interceptor.service';
import { AdminRoutingModule } from './admin/admin-routing.module';

@NgModule({
  providers: [
    AdminRoutingModule,
    PollService,
    VoteService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ]
})
export class DomainModule {}
