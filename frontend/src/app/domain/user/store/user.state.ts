import { Action, Selector, State, StateContext } from '@ngxs/store';
import { User } from '../user.model';
import * as UserActions from './user.actions';
import {UserService} from '../user.service';
import { of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

// TODO: selectedUsers, IUser
export interface UserModel {
  users: User[];
  selectedUser: User;
  editedUserIndex: number;
  fetching: boolean;
  loaded: boolean;
  error: boolean;
}

@State<UserModel>({
  name: 'userState',
  defaults: {
    users: [],
    selectedUser: null,
    editedUserIndex: -1,
    fetching: false,
    loaded: false,
    error: false,
  }
})
export class UserState {

  constructor(private userService: UserService) {}

  @Selector()
  static state(state: UserModel): UserModel {
    return state;
  }

  @Selector()
  static users(state: UserModel): User[] {
    return state.users;
  }

  @Selector()
  static user(state: UserModel): User {
    console.log(state);
    return state.selectedUser;
  }

  @Action(UserActions.FetchUsers)
  fetchUsers(ctx: StateContext<UserModel>) {

    ctx.patchState({ fetching: true, loaded: false, error: false });

    return this.userService.query()
      .pipe(
        map((payload) => {
            ctx.patchState({
              fetching: false, loaded: true, error: false,
              users: [...payload.body],
            });
        }),
        catchError(error => of(() => {
            ctx.patchState({ fetching: false, loaded: true, error: true });
        }))
      );
  }

  @Action(UserActions.AddUser)
  addUser(ctx: StateContext<UserModel>, { payload }: UserActions.AddUser) {
    return this.userService.create(payload)
      .pipe(
        map((response) => {
          ctx.patchState({
            users: [...ctx.getState().users, response],
            error: false
          });
        }),
        catchError(error => of(() => {
          ctx.patchState({ error: true });
        }))
      );
  }

  @Action(UserActions.UpdateUser)
  updateUser(ctx: StateContext<UserModel>, { payload }: UserActions.UpdateUser) {
    const state = ctx.getState();
    const user = state.users[state.editedUserIndex];
    const updatedUser = {
      ...user,
      ...payload
    };

    return this.userService.update(updatedUser)
      .pipe(
        map((response) => {
          const updatedUsers = [...state.users];
          updatedUsers[state.editedUserIndex] = updatedUser;

          ctx.patchState({
            users: updatedUsers,
            editedUserIndex: -1,
            selectedUser: null,
            error: false
          });
        }),
        catchError(error => of((e) => {
          ctx.patchState({ error: true });
        }))
      );
  }

  @Action(UserActions.DeleteUser)
  deleteUser(ctx: StateContext<UserModel>) {
    const state = ctx.getState();
    return this.userService.delete(state.selectedUser.id)
      .pipe(
        map((response) => {
          ctx.patchState({
            users: state.users.filter((u, uIndex) => {
              return uIndex !== state.editedUserIndex;
            }),
            editedUserIndex: -1,
            selectedUser: null,
          });
        }),
        catchError(error => of((e) => {
          ctx.patchState({ error: true });
        }))
      );
  }

  @Action(UserActions.SelectUser)
  selectUser(ctx: StateContext<UserModel>, { payload }: UserActions.SelectUser) {
    const state = ctx.getState();
    console.log('SelectUser ', payload);
    const userIndex = state.users.findIndex(u => u.id === +payload);
    console.log('SelectUser index ', userIndex);
    ctx.patchState({
      editedUserIndex: userIndex,
      selectedUser: { ...state.users[userIndex] },
    });
  }

  @Action(UserActions.UnselectUser)
  unselectUser(ctx: StateContext<UserModel>) {
    ctx.patchState({
      editedUserIndex: -1,
      selectedUser: null,
    });
  }

}
