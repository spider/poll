import { User } from '../user.model';

const FETCH_USERS = '[User] Fetch Users';
export const ADD_USER = '[User] Add User';
export const ADD_USERS = '[User] Add Users';
export const UPDATE_USER = '[User] Update User';
export const DELETE_USER = '[User] Delete User';
export const START_EDIT = '[User] Start Edit';
export const STOP_EDIT = '[User] Stop Edit';

export class FetchUsers {
  static readonly type = FETCH_USERS;

  constructor() {}
}

export class AddUser {
  static readonly type = ADD_USER;

  constructor(public payload: User) {}
}

export class AddUsers {
  static readonly type = ADD_USERS;

  constructor(public payload: User[]) {}
}

export class UpdateUser {
  static readonly type = UPDATE_USER;

  constructor(public payload: User ) {}
}

export class DeleteUser {
  static readonly type = DELETE_USER;
}

export class SelectUser {
  static readonly type = START_EDIT;

  constructor(public payload: number) {}
}

export class UnselectUser {
  static readonly type = STOP_EDIT;
}

export type UserActions =
  | AddUser
  | AddUsers
  | UpdateUser
  | DeleteUser
  | SelectUser
  | UnselectUser;
