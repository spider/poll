import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';


import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Poll} from './poll.model';
import {share, shareReplay} from 'rxjs/operators';

@Injectable()
export class PollService {
  public SERVER_API_URL = 'api';
  public resourceUrl = this.SERVER_API_URL + '/polls';

  pollsChanged = new Subject<Observable<Poll[]>>();

  constructor(protected http: HttpClient) {}

  getPolls(): Observable<Poll[]> {
    return this.http.get<Poll[]>(this.resourceUrl);
  }

  createPoll(firstName: string, poll: Poll) {
    console.log('create poll ' + firstName);
    const params = new HttpParams().set('userId', '1');
    return this.http.post<Poll>(this.resourceUrl, poll, {params}).subscribe(
      () => {
        this.pollsChanged.next(this.getPolls());
      }
    );
  }

  getPoll(id: number): Observable<Poll> {
    return this.http.get<Poll>(`${this.resourceUrl}/${id}`).pipe(shareReplay(1));
  }

  deletePoll(id: number) {
    return this.http.delete(`${this.resourceUrl}/${id}`).subscribe(
      () => {
        this.pollsChanged.next(this.getPolls());
      }
    );
  }

  updatePoll(id: number, poll: Poll) {
    const params = new HttpParams().set('userId', '1');
    return this.http.put<Poll>(this.resourceUrl, poll, {params}).subscribe(
      () => {
        this.pollsChanged.next(this.getPolls());
      }
    );
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<{}>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      (e) => this.onSaveError(e)
    );
  }

  protected onSaveSuccess(): void {
    // this.isSaving = false;
    // this.previousState();
  }

  protected onSaveError(e: any): void {
    console.log(e);
    // this.isSaving = false;
  }
}
