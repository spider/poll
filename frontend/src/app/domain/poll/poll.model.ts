import { Question, Vote } from './model/question.model';
import {User} from '../user/user.model';

export class Poll {
  constructor(
    public id: number,
    public name: string,
    public questions: Question[],
    // public createdBy: User,
    // public creationTimestamp: Date,
    public description?: string,
    public type?: string,
  ) {
  }
}

export class PollVotes {
  constructor(
    public pollId: number,
    public votes: Vote[],
  ) {}
}
