import { Component, Input, OnInit } from '@angular/core';

import { Poll } from '../../poll.model';

@Component({
  selector: 'app-poll-item',
  templateUrl: './poll-item.component.html',
  styleUrls: ['./poll-item.component.css']
})
export class PollItemComponent implements OnInit {
  @Input() poll: Poll;

  ngOnInit() {
  }
}
