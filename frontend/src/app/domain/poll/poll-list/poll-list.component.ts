import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {Observable } from 'rxjs';


import { PollService } from '../poll.service';
import { Poll } from '../poll.model';

@Component({
  selector: 'app-poll-list',
  templateUrl: './poll-list.component.html',
  styleUrls: ['./poll-list.component.css']
})
export class PollListComponent implements OnInit {

  polls$: Observable<Poll[]>;

  constructor(private pollService: PollService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.polls$ = this.pollService.getPolls();
    this.pollService.pollsChanged.subscribe(
      (polls) => {
        this.polls$ = polls;
      }
    );
  }

  onNewPoll() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
