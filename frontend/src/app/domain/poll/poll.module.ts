import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { PollComponent } from './poll.component';
import { PollListComponent } from './poll-list/poll-list.component';
import { PollDetailComponent } from './poll-detail/poll-detail.component';
import { PollItemComponent } from './poll-list/poll-item/poll-item.component';
import { PollStartComponent } from './poll-start/poll-start.component';
import { PollEditComponent } from './poll-edit/poll-edit.component';
import { PollsRoutingModule } from './polls-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    PollComponent,
    PollListComponent,
    PollDetailComponent,
    PollItemComponent,
    PollStartComponent,
    PollEditComponent
  ],
  imports: [
    RouterModule,
    ReactiveFormsModule,
    PollsRoutingModule,
    SharedModule
  ]
})
export class PollModule {}
