import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Poll, PollVotes } from '../poll.model';
import { PollService } from '../poll.service';
import { Observable } from 'rxjs';
import { VoteService } from '../vote.service';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Question, Vote } from '../model/question.model';

@Component({
  selector: 'app-poll-detail',
  templateUrl: './poll-detail.component.html',
  styleUrls: ['./poll-detail.component.css']
})
export class PollDetailComponent implements OnInit {

  pollForm: FormGroup;
  poll$: Observable<Poll>;
  id: number;

  constructor(private pollService: PollService,
              private voteService: VoteService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          this.initForm();
        }
      );
  }

  onEditPoll() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeletePoll() {
    this.pollService.deletePoll(this.id);
    this.router.navigate(['/poll']);
  }

  onVote(pollVotes: PollVotes) {
    this.voteService.voteForPollOption(pollVotes);
    this.router.navigate(['/poll']);
  }

  private initForm() {
    this.pollForm = new FormGroup({
      questions: new FormArray([])
    });

    this.poll$ = this.pollService.getPoll(this.id);

    this.poll$.subscribe(poll => {
      (poll.questions || []).forEach(question => this.addQuestion(question));
    });
  }

  private addQuestion(question: Question) {
    const questionId = question !== null ? question.id : null;

    (this.pollForm.get('questions') as FormArray).push(
      new FormGroup({
        questionId: new FormControl(questionId, Validators.required),
        selectedAnswer: new FormControl('', Validators.required)
      })
    );
  }

  onSubmit() {
    console.log(this.pollForm.value);
    const pollVotes: PollVotes = new PollVotes(this.id, []);
    this.pollForm.value.questions
      .forEach(q => pollVotes.votes.push(new Vote(q.questionId, q.selectedAnswer)));
    this.onVote(pollVotes);
  }
}
