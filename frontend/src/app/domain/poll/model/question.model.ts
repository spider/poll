import { AnswerOption } from './answer.option.model';

export class Question {
  constructor(
    public id: number,
    public question: string,
    public answerOptions: AnswerOption[],
  ) {}
}

export class Vote {
  constructor(
    public questionId: number,
    public answerId: number,
  ) {}
}
