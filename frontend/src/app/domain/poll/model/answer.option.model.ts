export class AnswerOption {
  constructor(
    public id: number,
    public answer: string,
    public votes?: number
  ) {}
}
