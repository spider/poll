import { Poll } from '../poll.model';

export const ADD_POLL = '[Poll] Add Poll';
export const ADD_POLLS = '[Poll] Add Polls';
export const UPDATE_POLL = '[Poll] Update Poll';
export const DELETE_POLL = '[Poll] Delete Poll';
export const START_EDIT = '[Poll] Start Edit';
export const STOP_EDIT = '[Poll] Stop Edit';

export class AddPoll {
  static readonly type = ADD_POLL;

  constructor(public payload: Poll) {}
}

export class AddPolls {
  static readonly type = ADD_POLLS;

  constructor(public payload: Poll[]) {}
}

export class UpdatePoll {
  static readonly type = UPDATE_POLL;

  constructor(public payload: Poll ) {}
}

export class DeletePoll {
  static readonly type = DELETE_POLL;
}

export class StartEdit {
  static readonly type = START_EDIT;

  constructor(public payload: number) {}
}

export class StopEdit {
  static readonly type = STOP_EDIT;
}

export type PollActions =
  | AddPoll
  | AddPolls
  | UpdatePoll
  | DeletePoll
  | StartEdit
  | StopEdit;
