import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Poll } from '../poll.model';
import * as UserActions from './poll.actions';

export interface UserModel {
  users: Poll[];
  editedUser: Poll;
  editedUserIndex: number;
}

@State<UserModel>({
  name: 'userState',
  defaults: {
    users: [],
    editedUser: null,
    editedUserIndex: -1,
  }
})
export class PollState {
  @Selector()
  static state(state: UserModel): UserModel {
    return state;
  }

  @Selector()
  static users(state: UserModel): Poll[] {
    return state.users;
  }

  @Action(UserActions.AddPoll)
  addIngredient(
    ctx: StateContext<UserModel>,
    { payload }: UserActions.AddPoll
  ) {
    ctx.patchState({
      users: [...ctx.getState().users, payload],
    });
  }

  @Action(UserActions.AddPolls)
  addIngredients(
    ctx: StateContext<UserModel>,
    { payload }: UserActions.AddPolls
  ) {
    console.log('!!! addIngredients');
    ctx.patchState({
      users: [...ctx.getState().users, ...payload],
    });
  }

  @Action(UserActions.UpdatePoll)
  updateIngredient(
    ctx: StateContext<UserModel>,
    { payload }: UserActions.UpdatePoll
  ) {
    const state = ctx.getState();
    const ingredient = state.users[state.editedUserIndex];
    const updatedUser = {
      ...ingredient,
      ...payload
    };
    const updatedUsers = [...state.users];
    updatedUsers[state.editedUserIndex] = updatedUser;

    ctx.patchState({
      users: updatedUsers,
      editedUserIndex: -1,
      editedUser: null,
    });
  }

  @Action(UserActions.DeletePoll)
  deleteIngredient(
    ctx: StateContext<UserModel>,
  ) {
    const state = ctx.getState();
    ctx.patchState({
      users: state.users.filter((u, uIndex) => {
        return uIndex !== state.editedUserIndex;
      }),
      editedUserIndex: -1,
      editedUser: null,
    });
  }

  @Action(UserActions.StartEdit)
  StartEdit(
    ctx: StateContext<UserModel>,
    { payload }: UserActions.StartEdit
  ) {
    const state = ctx.getState();
    ctx.patchState({
      editedUserIndex: payload,
      editedUser: { ...state.users[payload] },
    });
  }

  @Action(UserActions.StopEdit)
  StopEdit(
    ctx: StateContext<UserModel>,
  ) {
    ctx.patchState({
      editedUserIndex: -1,
      editedUser: null,
    });
  }
}
