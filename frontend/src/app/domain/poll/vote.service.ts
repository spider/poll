import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


import {HttpClient, HttpParams} from '@angular/common/http';
import {Poll, PollVotes} from './poll.model';

@Injectable()
export class VoteService {
  public SERVER_API_URL = 'api';
  public resourceUrl = this.SERVER_API_URL + '/votes';

  constructor(protected http: HttpClient) {}

  voteForPollOption(pollVotes: PollVotes) {
    const params = new HttpParams()
      .set('pollId', String(pollVotes.pollId));

    return this.http.post<Poll>(this.resourceUrl, pollVotes.votes, {params}).subscribe();
  }
}
