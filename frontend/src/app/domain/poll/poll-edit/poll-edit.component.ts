import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormGroup, FormControl, FormArray, Validators, FormBuilder, AbstractControl} from '@angular/forms';

import {PollService} from '../poll.service';
import {AnswerOption} from '../model/answer.option.model';
import {Question} from '../model/question.model';
import {AccountService} from '../../../security/authentication/services/account.service';
import {Observable} from 'rxjs';
import {PrincipalAccount} from '../../../security/authentication/account.model';

@Component({
  selector: 'app-poll-edit',
  templateUrl: './poll-edit.component.html',
  styleUrls: ['./poll-edit.component.css']
})
export class PollEditComponent implements OnInit {
  id: number;
  editMode = false;
  pollForm: FormGroup;
  private principal: PrincipalAccount;


  get questionsControls() {
    return (this.pollForm.get('questions') as FormArray).controls;
  }

  constructor(
    private accountService: AccountService,
    private route: ActivatedRoute,
    private pollService: PollService,
    private router: Router,
    private fb: FormBuilder,
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.editMode = params['id'] != null;
      this.initForm();
    });
    this.accountService.getPrincipal().subscribe( (a) => {
      this.principal = a;
    });
  }

  onSubmit() {
    // TODO: add edit with cleanup for votes
    // if (this.editMode) {
    //   this.pollService.updatePoll(this.id, this.pollForm.value);
    // } else {
      this.pollService.createPoll(this.principal.firstName, this.pollForm.value);
    // }
    this.onCancel();
  }

  onAddQuestion() {
    this.addQuestion(null);
  }

  onDeleteQuestion(index: number) {
    this.deleteQuestion(index);
  }

  onAddAnswer(answers: AbstractControl) {
    (answers as FormArray).push(
      new FormGroup({
        id: new FormControl(null),
        answer: new FormControl('', Validators.required),
        votes: new FormControl(0),
      })
    );
  }

  onDeleteAnswer(answers, index: number) {
    (answers as FormArray).removeAt(index);
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private initForm() {
    this.pollForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl(''),
      questions: new FormArray([])
    });

    if (this.editMode) {
      this.pollService.getPoll(this.id).subscribe(poll => {
        this.pollForm.get('name').setValue(poll.name);
        this.pollForm.get('description').setValue(poll.description);
        (poll.questions || []).forEach(question => this.addQuestion(question));
      });
    } else {
      this.addQuestion(null);
    }
  }

  private addQuestion(question: Question) {
    const questionName = question !== null ? question.question : null;

    const answers = new FormArray([]);
    if (question !== null && question.answerOptions !== null) {
      question.answerOptions.forEach(answerOption => answers.push(
        new FormGroup({
          id: new FormControl(answerOption.id),
          answer: new FormControl(answerOption.answer),
          votes: new FormControl(answerOption.votes ? answerOption.votes : 0),
        })
      ));
    }

    (this.pollForm.get('questions') as FormArray).push(
      new FormGroup({
        question: new FormControl(questionName, Validators.required),
        answerOptions: answers,
      })
    );
  }

  private deleteQuestion(index: number) {
    (this.pollForm.get('questions') as FormArray).removeAt(index);
  }
}
