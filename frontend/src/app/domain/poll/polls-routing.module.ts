import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PollComponent } from './poll.component';
import { AuthenticationGuard } from '../../security/guards/authentication-guard.service';
import { PollStartComponent } from './poll-start/poll-start.component';
import { PollEditComponent } from './poll-edit/poll-edit.component';
import { PollDetailComponent } from './poll-detail/poll-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PollComponent,
    canActivate: [AuthenticationGuard],
    children: [
      { path: '', component: PollStartComponent },
      { path: 'new', component: PollEditComponent },
      {
        path: ':id',
        component: PollDetailComponent
      },
      {
        path: ':id/edit',
        component: PollEditComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PollsRoutingModule {}
