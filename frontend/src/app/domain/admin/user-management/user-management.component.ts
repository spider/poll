import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { DeleteUser, FetchUsers, SelectUser, UserState } from 'src/app/store/app.state';
import { User } from '../../user/user.model';

@Component({
  selector: 'app-user-mgmt',
  templateUrl: './user-management.component.html'
})
export class UserManagementComponent implements OnInit {

  @Select(UserState.users)
  users$: Observable<User[]>;

  constructor(
    private store: Store,
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new FetchUsers());
  }

  // TODO: Add confirm dialog
  deleteUser(user: User): void {
    this.store.dispatch(new SelectUser(user.id));
    this.store.dispatch(new DeleteUser());
  }
}
