import { Component, OnInit } from '@angular/core';
import { User } from '../../user/user.model';
import {UserState} from '../../user/store/user.state';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-user-mgmt-detail',
  templateUrl: './user-management-detail.component.html'
})
export class UserManagementDetailComponent {

  @Select(UserState.user)
  user$: Observable<User>;

  constructor() {
    // TODO: replace with form
  }
}
