import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { UserManagementComponent } from './user-management.component';
import { UserManagementDetailComponent } from './user-management-detail.component';
import { UserManagementUpdateComponent } from './user-management-update.component';
import {UserService} from '../../user/user.service';
import {Store} from '@ngxs/store';
import * as UsersActions from '../../user/store/user.actions';

@Injectable({ providedIn: 'root' })
export class UserManagementResolve implements Resolve<void> {

  constructor(private service: UserService, private store: Store) {}

  resolve(route: ActivatedRouteSnapshot): Observable<void> {
    const id = route.params.id;

    return this.store.dispatch(id ?
      new UsersActions.SelectUser(id) :
      new UsersActions.UnselectUser());
  }
}

export const userManagementRoute: Routes = [
  {
    path: '',
    component: UserManagementComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      defaultSort: 'id,asc'
    }
  },
  {
    path: ':id/view',
    component: UserManagementDetailComponent,
    resolve: {
      user: UserManagementResolve
    }
  },
  {
    path: 'new',
    component: UserManagementUpdateComponent,
    resolve: {
      user: UserManagementResolve
    }
  },
  {
    path: ':id/edit',
    component: UserManagementUpdateComponent,
    resolve: {
      user: UserManagementResolve
    }
  }
];
