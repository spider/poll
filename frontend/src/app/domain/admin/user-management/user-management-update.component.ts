import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import {IUser, User} from '../../user/user.model';
import {UserModel, UserState} from '../../user/store/user.state';
import {AddUser, SelectUser, UpdateUser} from '../../user/store/user.actions';


@Component({
  selector: 'app-user-mgmt-update',
  templateUrl: './user-management-update.component.html'
})
export class UserManagementUpdateComponent implements OnInit {

  @Select(UserState.user)
  user$: Observable<User>;

  user: User = new User(null, null);

  userForm = this.fb.group({
    id: [],
    username: ['',
      [ Validators.required,  Validators.pattern('^[_.@A-Za-z0-9-]*'),
        Validators.minLength(1), Validators.maxLength(50)] ],
    firstName: ['',
      [ Validators.minLength(1), Validators.maxLength(50)] ],
    lastName: ['',
      [ Validators.minLength(1), Validators.maxLength(50)] ],
    email: ['',
      [ Validators.required, Validators.email,
        Validators.minLength(5), Validators.maxLength(254)]],
    activated: [],
    authorities: []
  });

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private store: Store,
              ) {
    // TODO: add select action by router id
  }

  ngOnInit(): void {
    // this.store.dispatch(new SelectUser(this.route.snapshot.params.id));
    this.user$.subscribe(( user ) => {
      if (user) {
        this.user = user;
        this.userForm.patchValue(this.updateForm(this.user));
      }
    });
  }

  private updateForm(user: User): IUser {
    return {
      id: user.id,
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      activated: user.activated,
      authorities: user.authorities
    };
  }

  save(): void {
    let response: Observable<UserModel>;

    if (!this.user.id) {
      response = this.store.dispatch(new AddUser(this.updateUser(this.user)));
    } else {
      response = this.store.dispatch(new UpdateUser(this.updateUser(this.user)));
    }

    response.subscribe((r) => {
      if (!r.error) {
        this.previousState();
      }
    });
  }

  // TODO: return user from this and updated
  private updateUser(user: User): User {
    const uuser = {...this.user};
    uuser.username = this.userForm.get(['username']).value;
    uuser.password = 'admin';
    uuser.firstName = this.userForm.get(['firstName']).value;
    uuser.lastName = this.userForm.get(['lastName']).value;
    uuser.email = this.userForm.get(['email']).value;
    uuser.activated = this.userForm.get(['activated']).value;
    uuser.authorities = this.userForm.get(['authorities']).value;
    return uuser;
  }

  previousState(): void {
    window.history.back();
  }
}
