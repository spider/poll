import {Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-signin',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  isLoginMode = true;
  error: string = null;

  constructor(
    private loginService: LoginService,
    private router: Router,
  ) {}

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }

    if (this.isLoginMode) {
      this.loginService.login({
        username: form.value.email,
        password: form.value.password,
        rememberMe: false
      })
        .subscribe(
          () => {
            this.error = null;
            if (
              this.router.url === '/account/register' ||
              this.router.url.startsWith('/account/activate') ||
              this.router.url.startsWith('/account/reset/')
            ) {
              this.router.navigate(['']);
            } else {
              this.router.navigate(['/poll']);
            }
          },
          (errorMessage) => {
            console.log(errorMessage);
            this.error = errorMessage;
          }
        );
      // authObs = this.authService.login(email, password);
    } else {
      // authObs = this.authService.signup(email, password);
    }

    // authObs.subscribe(
    //   resData => {
    //     console.log(resData);
    //     this.isLoading = false;
    //     this.router.navigate(['/poll']);
    //   },
    //   errorMessage => {
    //     console.log(errorMessage);
    //     this.error = errorMessage;
    //     this.isLoading = false;
    //   }
    // );

    form.reset();
  }

  ngOnInit(): void {
  }
}
