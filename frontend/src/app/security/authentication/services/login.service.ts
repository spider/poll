import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {flatMap, tap} from 'rxjs/operators';

import {PrincipalAccount} from '../account.model';
import { AccountService } from './account.service';
import { AuthenticationService, LOGOUT_URL } from './authentication.service';

export class Login {
  constructor(
    public username: string,
    public password: string,
    public rememberMe: boolean
  ) {}
}

@Injectable({ providedIn: 'root' })
export class LoginService {

  constructor(
    private accountService: AccountService,
    private authServerProvider: AuthenticationService
  ) {}

  login(credentials: Login): Observable<PrincipalAccount | null> {
    return this.authServerProvider.login(credentials).pipe(
      flatMap(() => this.accountService.identity())
    );
  }

  logout(): void {
    this.authServerProvider.logout().pipe(
        tap(() => this.accountService.setPrincipal(null))
      ).subscribe();
  }
}
