import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {Observable, ReplaySubject, of, BehaviorSubject} from 'rxjs';
import { shareReplay, tap, catchError } from 'rxjs/operators';

import { SERVER_API_URL } from '../../../app.constants';
import { PrincipalAccount } from '../account.model';

@Injectable({ providedIn: 'root' })
export class AccountService {
  private principal$ = new BehaviorSubject<PrincipalAccount>(null);
  private isAuthenticated = false;
  public SERVER_API_URL = 'api';
  private accountCache$: Observable<PrincipalAccount | null>;

  constructor(private http: HttpClient) {
  }

  setPrincipal(identity: PrincipalAccount): void {
    this.isAuthenticated = !!identity;
    this.principal$.next(identity);
  }

  identity(): Observable<PrincipalAccount | null> {
    if (!this.isAuthenticated) {
      this.accountCache$ = this.fetch().pipe(
        catchError(() => {
          this.setPrincipal(null);
          return of(null);
        }),
        tap((account: PrincipalAccount | null) => {
          this.setPrincipal(account);
        }),
        shareReplay()
      );
    }
    return this.accountCache$;
  }

  getPrincipal(): Observable<PrincipalAccount> {
    return this.principal$.asObservable();
  }

  private fetch(): Observable<PrincipalAccount> {
    this.isAuthenticated = false;
    return this.http.get<PrincipalAccount>('api/account');
  }
}
