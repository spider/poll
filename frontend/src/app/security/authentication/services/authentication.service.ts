import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from './login.service';



export const LOGOUT_URL = 'api' + '/logout';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private SERVER_API_URL = 'api';

  constructor(private http: HttpClient) {}

  login(credentials: Login): Observable<{}> {
    const data =
      `username=${encodeURIComponent(credentials.username)}` +
      `&password=${encodeURIComponent(credentials.password)}` +
      `&remember-me=${credentials.rememberMe}` +
      '&submit=Login';

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(this.SERVER_API_URL + '/login', data, { headers });
  }

  logout(): Observable<void> {
    // logout from the server
    return this.http.post(LOGOUT_URL, {})
      .pipe(
      map(() => {
        // to get a new csrf token call the api
        this.http.get(this.SERVER_API_URL + '/account').subscribe();
      })
    );
  }
}
