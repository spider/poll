import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpParams
} from '@angular/common/http';
import {take, exhaustMap, map} from 'rxjs/operators';

import {AccountService} from '../authentication/services/account.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private accountService: AccountService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.accountService.getPrincipal().pipe(
      take(1),
      exhaustMap(principal => {
        if (true || !principal) {
          return next.handle(req);
        }
        const modifiedReq = req.clone({
          params: new HttpParams().set('auth', 'principal.token')
        });
        return next.handle(modifiedReq);
      })
    );
  }
}
