import { NgModule, LOCALE_ID } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpClientXsrfModule} from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { CookieModule } from 'ngx-cookie';
import { NgxWebstorageModule } from 'ngx-webstorage';

import { AuthCookieInterceptor } from './interceptor/auth-cookie.interceptor';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { SignUpComponent } from './authentication/signup/sign-up.component';
import { SignInComponent } from './authentication/signin/sign-in.component';
import { AuthenticationComponent } from './authentication/authentication.component';

@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    AuthenticationComponent,
  ],
  imports: [
    HttpClientModule,
    HttpClientXsrfModule,
    CookieModule.forRoot(),
    NgxWebstorageModule.forRoot({prefix: 'jhi', separator: '-'}),
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: AuthenticationComponent }]),
    SharedModule,
    // NgJhipsterModule.forRoot({
    //   // set below to true to make alerts look like toast
    //   alertAsToast: false,
    //   alertTimeout: 5000
    // })
  ],
  providers: [
    Title,
    {
      provide: LOCALE_ID,
      useValue: 'en'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthCookieInterceptor,
      multi: true
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: NotificationInterceptor,
    //   multi: true
    // }
  ]
})
export class SecurityModule {
}
